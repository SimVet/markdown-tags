// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { MarkdownTagsProvider } from './markdownTags';
import { FilesForTagProvider } from './filesForTag';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
	const tagsProvider = new MarkdownTagsProvider(context);
	vscode.window.registerTreeDataProvider("markdownTags", tagsProvider);

	const filesForTagProvider = new FilesForTagProvider();
	vscode.window.registerTreeDataProvider("filesForTag", filesForTagProvider);

	vscode.commands.registerCommand("markdownTags.showTag", (tag: string) => {
		filesForTagProvider.forTag(tag);
	});
	vscode.commands.registerCommand("markdownTags.refresh", (tag: string) => {
		tagsProvider.refresh();
		filesForTagProvider.refresh();
	});
	vscode.commands.registerCommand('markdownTags.openFile', (resource: vscode.Uri) => {
		vscode.window.showTextDocument(resource);
	});
}

// this method is called when your extension is deactivated
export function deactivate() { }
