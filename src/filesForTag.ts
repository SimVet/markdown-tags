import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';
import * as glob from 'glob';
import * as yaml from 'js-yaml';
import { getTagsForFile } from './markdownTags';

export class FilesForTagProvider implements vscode.TreeDataProvider<MarkdownFile> {
    private _onDidChangeTreeData: vscode.EventEmitter<MarkdownFile | undefined> = new vscode.EventEmitter<MarkdownFile | undefined>();
    readonly onDidChangeTreeData: vscode.Event<MarkdownFile | undefined> = this._onDidChangeTreeData.event;

    constructor() {
    }

    get workspaceRoot() {
        return vscode.workspace.rootPath || "";
    }

    private filterTag: string | null = null;

    /**
     * Update the tag for which files are beeing displayed
     * @param tag 
     */
    forTag(tag: string) {
        this.filterTag = tag;
        this.refresh();
    }

    refresh(): void {
        this._onDidChangeTreeData.fire();
    }

    getTreeItem(element: MarkdownFile): vscode.TreeItem {
        return element;
    }

    async getChildren(element?: MarkdownFile): Promise<MarkdownFile[]> {
        let files = await this.getMarkdownFiles();
        return files.filter(file => {
            if (this.filterTag == null) return true;
            const fullPath = path.join(this.workspaceRoot, file);
            let tags = getTagsForFile(fullPath);
            for (let tag of tags) {
                if (tag.startsWith(this.filterTag)) return true;
            }
            return false;
        }).map(file => {
            return new MarkdownFile(file, vscode.TreeItemCollapsibleState.None, {
                command: "markdownTags.openFile",
                arguments: [vscode.Uri.file(path.join(this.workspaceRoot, file))],
                title: "show in editor"
            })
        })
    }

    /**
     * get list of markdown files that can be inspected to get the MarkdownFiles
     */
    getMarkdownFiles(): Promise<string[]> {
        return new Promise((resolve, reject) => {
            glob("**/*.md", {
                cwd: this.workspaceRoot,
                root: this.workspaceRoot
            }, (err, matches) => {
                if (err) return reject(err);
                resolve(matches)
            })
        });
    }
}

export class MarkdownFile extends vscode.TreeItem {
    constructor(
        public readonly label: string,
        public readonly collapsibleState: vscode.TreeItemCollapsibleState,
        public readonly command?: vscode.Command
    ) {
        super(label, collapsibleState);
    }

    public files: string[] = [];
    public children: { [name: string]: MarkdownFile } = {};

    get tooltip(): string {
        return `${this.label}`;
    }

    get description(): string {
        return ""
    }

    iconPath = vscode.ThemeIcon.File

    contextValue = 'file';
}