import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';
import * as glob from 'glob';
import * as yaml from 'js-yaml';

export function getTagsForFile(filePath: string): string[] {
    let file = fs.readFileSync(filePath, { encoding: "utf-8" });
    try {
        if (!file.startsWith("---")) { return []; }
        let i = file.indexOf("---", 4);
        if (i < 0) { return []; }
        file = file.slice(4, i);
        let doc = yaml.safeLoad(file);
        return doc.tags;
    } catch (e) { }
    return [];
}

export class MarkdownTagsProvider implements vscode.TreeDataProvider<Tag> {
    private _onDidChangeTreeData: vscode.EventEmitter<Tag | undefined> = new vscode.EventEmitter<Tag | undefined>();
    readonly onDidChangeTreeData: vscode.Event<Tag | undefined> = this._onDidChangeTreeData.event;

    /** The icon that is used to identify tags */
    private readonly icon: { light: string; dark: string; };

    constructor(private context: vscode.ExtensionContext) {
        this.icon = {
            light: this.context.asAbsolutePath(path.join('media', 'light', 'label.svg')),
            dark: this.context.asAbsolutePath(path.join('media', 'dark', 'label.svg'))
        };
    }

    get workspaceRoot() {
        return vscode.workspace.rootPath || "";
    }

    refresh(): void {
        this._onDidChangeTreeData.fire();
    }

    getTreeItem(element: Tag): vscode.TreeItem {
        return element;
    }

    async getChildren(element?: Tag): Promise<Tag[]> {
        if (element) {
            return element.tree.getChildren();
        }
        if (!this.workspaceRoot) {
            vscode.window.showInformationMessage('No Tag in empty workspace');
            return Promise.resolve([]);
        }

        const files = await this.getMarkdownFiles();

        let tn = new TagTree("", this.icon);

        files.forEach(match => {
            const fullPath = path.join(this.workspaceRoot, match);
            try {
                let fileTags = getTagsForFile(fullPath);
                for (let tag of fileTags) {
                    let parts = tag.split("/");
                    let cp = tn;
                    let name = "";
                    for (let part of parts) {
                        if (name !== "") {
                            name += "/" + part;
                        } else {
                            name = part;
                        }
                        if (cp.children[part]) {
                            cp = cp.children[part];
                        } else {
                            cp = cp.children[part] = new TagTree(name, this.icon);
                        }
                    }
                    cp.files.push(match);
                }
            } catch (err) {
                console.log(err);
                return;
            }
        });
        return tn.getChildren();
    }

    /**
     * get list of markdown files that can be inspected to get the tags
     */
    getMarkdownFiles(): Promise<string[]> {
        return new Promise((resolve, reject) => {
            glob("**/*.md", {
                cwd: this.workspaceRoot,
                root: this.workspaceRoot
            }, (err, matches) => {
                if (err) { return reject(err); }
                resolve(matches);
            });
        });
    }
}

class TagTree {
    constructor(public readonly name: string, private readonly icon: { light: string, dark: string }) { }
    public children: { [name: string]: TagTree } = {};
    public files: string[] = [];

    private _tag?: Tag;
    get tag() {
        if (this._tag) { return this._tag; }

        let c = vscode.TreeItemCollapsibleState.None;
        if (Object.keys(this.children).length > 0) {
            c = vscode.TreeItemCollapsibleState.Collapsed;
        }
        const parts = this.name.split("/");
        return this._tag = new Tag(parts[parts.length - 1], c, this, {
            command: 'markdownTags.showTag',
            title: 'Show',
            arguments: [this.name]
        }, this.numFiles.toString(), this.icon);
    }

    get numFiles(): number {
        const children: TagTree[] = Object.keys(this.children).map(key => this.children[key]);
        return this.files.length + children.reduce((sum: number, child: TagTree) => sum + child.numFiles, 0);
    }

    getChildren(): Tag[] {
        return Object.keys(this.children).map(key => this.children[key].tag);
    }
}

export class Tag extends vscode.TreeItem {
    constructor(
        public readonly label: string,
        public readonly collapsibleState: vscode.TreeItemCollapsibleState,
        public readonly tree: TagTree,
        public readonly command?: vscode.Command,
        public readonly description?: string,
        public readonly iconPath?: { light: string, dark: string }
    ) {
        super(label, collapsibleState);
    }

    public files: string[] = [];
    public children: { [name: string]: Tag } = {};

    get tooltip(): string {
        return `${this.label}`;
    }

    contextValue = 'tag';
}
